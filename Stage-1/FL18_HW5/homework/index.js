function isEqual(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a > b;
}

function storeNames(...str) {
    return str;
}

function getDifference(a, b) {
    return a >= b ? a - b : b - a;
}

function negativeCount(arr) {
    let negatives = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            negatives += 1;
        }
    }
    return negatives;
}

function letterCount(str1, str2) {
    return str1.split(str2).length - 1;
}

function countPoints(points) {
    const addPoint = (a, [b, c]) => {
        if (b > c) {
            a = a + 3;
            return a;
        } else if (b < c) {
            a = a + 0;
            return a;
        } else {
            a = a + 1;
            return a;
        }
    }
    return points.map((a) => a.split(':').map(b => +b))
        .reduce(addPoint, 0);
}