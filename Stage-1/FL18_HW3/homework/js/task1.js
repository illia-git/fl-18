const inputMoney = +prompt('Input initial amount of money.'); 
const inputYears = +prompt('Input number of years.'); 
const inputPercent = +prompt('Input percentage of a year.'); 
 
(function validateData() { 
    const minMoney = 1000, minYears = 1, maxPercent = 100, minPercent = 1; 
    if (isNaN(inputMoney) || isNaN(inputYears) || isNaN(inputPercent)) { 
        alert('You can input numbers only!'); 
        window.location.reload(); 
    } else if (inputMoney < minMoney || inputYears < minYears || 
        inputPercent > maxPercent || inputPercent < minPercent) { 
        alert('Min money: 1000.\nMin years: 1.\nMax percentage: 100.\nMin percentage: 1.'); 
        window.location.reload(); 
    } else { 
        alert('Data accepted'); 
        calcProfit(); 
    } 
    return; 
})(); 
 
// формула сложных процентов 
function calcPercentFormula(m, p, y) { //m-money p-percent y-year 
    const base = 1 + p / 100; 
    const exponent = y; 
    let result = m * Math.pow(base, exponent); 
    return result.toFixed(2); 
} 
 
function calcProfit() { 
    const totalAmount = calcPercentFormula(inputMoney, inputPercent, inputYears); 
    const totalProfit = (totalAmount - inputMoney).toFixed(2); 
    alert(`Result:  \nInitial amount: ${inputMoney} 
                    \nNumber of years: ${inputYears} 
                    \nPercentage of year: ${inputPercent} 
                    \nTotal profit: ${totalProfit} 
                    \nTotal amount: ${totalAmount}`); 
    return; 
}