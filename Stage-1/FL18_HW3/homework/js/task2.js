const inputAnswer = confirm('Do you want to play a game?');
const maxAttempts = 3;
const minInt = 0;
let maxInt = 1;
let attemptCounter = 1, prize = 100, levelCounter = 1, bank = 0;

(function checkAnswer() {
    if (!inputAnswer) {
        alert('You did not become a billionaire, but you always can!');
    } else {
        startGame();
    }
    return;
})();

// функция получения рандомного числа в диапазоне x-y
function getRandomInt(x, y) { //x-min y-max
    x = Math.ceil(x);
    y = Math.floor(y);
    return Math.floor(Math.random() * (y - x) + x);
}

function startGame() {
    const ourNumber = getRandomInt(minInt, maxInt);
    const inputNumber = parseInt(prompt(`You are on level ${levelCounter}\nGuess the number between 0 and ${maxInt}
    \nYour attempt: ${attemptCounter}
    \nAttempts left: ${maxAttempts - attemptCounter}\nYour current balance: ${bank}$\nYou can win: ${prize}$`));
    if (isNaN(inputNumber)) {
        cancelGame();
    } else if (inputNumber === ourNumber) {
        const nextLevelAnswer = confirm(`Congratulation, you won! Your prize is: ${prize}$. Do you want to continue?`)
        nextLevelAnswer === true ? nextLevel() : cancelGame();
    } else if (attemptCounter < maxAttempts) {
        prize /= 2;
        attemptCounter++;
        const nextAnswer = confirm('You lose. Do you want to play again?');
        nextAnswer === true ? startGame() : cancelGame();
    } else if (attemptCounter >= maxAttempts) {
        const finalLose = confirm('You lose! Do you want to play again?');
        finalLose === true ? window.location.reload() : cancelGame();
    }
    return;
}

function nextLevel() {
    bank += prize;
    levelCounter++;
    prize *= levelCounter;
    attemptCounter = 1;
    maxInt += 1;
    alert(`This is a ${levelCounter} level!`);
    getRandomInt(minInt, maxInt);
    startGame();
    return;
}

function cancelGame() {
    alert('You cancelled the game.');
    window.location.reload()
    return;
}