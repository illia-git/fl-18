function reverseNumber(num) {
    const t = true;
    const f = false;
    const isNegative = num < 0 ? t : f;
    if (isNegative) {
        num = num * -1;
    }
    let reversed = 0;
    while (num > 0) {
        reversed = reversed * 10 + num % 10;
        num = parseInt(num / 10);
    }
    return isNegative ? reversed * -1 : reversed;
}

function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}

function map(arr, func) {
    let newArray = [];
    forEach(arr, (item) => {
        newArray.push(func(item))
    });
    return newArray;
}

function filter(arr, func) {
    let newArray = [];
    forEach(arr, (item) => {
        if (func(item)) {
            newArray.push(item)
        }
    });
    return newArray;
}

function getAdultAppleLovers(data) {
    const minAge = 18;
    const fruit = 'apple';
    let result = [];
    filter(data, (item) => {
        if (item.age >= minAge && item.favoriteFruit === fruit) {
            result.push(item.name)
            return result;
        }
    });
}

function getKeys(obj) {
    let allKeys = [];
    for (let key in obj) {
        allKeys.push(key)
    }
    return allKeys;
}

function getValues(obj) {
    let allValues = [];
    for (let key in obj) {
        allValues.push(obj[key])
    }
    return allValues;
}

function showFormattedDate(dateObj) {
    const dateArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const d = dateObj.getDate();
    const m = dateArray[dateObj.getMonth()];
    const y = dateObj.getFullYear();
    const dateString = `It is ${d} of ${m}, ${y}`;
    return dateString;
}