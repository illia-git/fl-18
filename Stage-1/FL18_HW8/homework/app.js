const appRoot = document.getElementById('app-root');
const REGIONS_LIST = externalService.getRegionsList();
const LANGUAGES_LIST = externalService.getLanguagesList();

const createHeader = `<h1 class="header-title">Countries Search</h1>`;
appRoot.innerHTML += createHeader;

const createRadioBtns = `
    <form class="search">
        <legend>Please choose the type of search:</legend>
        <ul class="list">
            <li class="list-item">
                <input type="radio" id="region" onclick="selectRegions()" name="name">
                <label for="region">By Region</label>
            </li>
            <li class="list-item">
                <input type="radio" id="lang" onclick="selectLanguages()" name="name">
                <label for="lang">By Language</label>
            </li>
        </ul>
    </form>`;
appRoot.innerHTML += createRadioBtns;

const createSelect = `
    <form class="search">
        <legend>Please choose search query:</legend>
            <ul class="list">
                <li class="list-item">
                    <select id="select" onchange="selectOption()" disable="true">
                        <option id="option">Select value</option>
                    </select>
                </li>
            </ul>
    </form>`;
appRoot.innerHTML += createSelect;

let regionsORlanguages = '';
function selectRegions() {
    for (const value of externalService.getLanguagesList()) {
        const option = document.createElement('option');
        option.textContent = option.value = value;
        select.remove(option);
    }
    for (const value of externalService.getRegionsList()) {
        const option = document.createElement('option');
        option.textContent = option.value = value;
        select.append(option);
    }
    regionsORlanguages = 'regions';
    return regionsORlanguages;
}

function selectLanguages() {
    for (const value of externalService.getRegionsList()) {
        const option = document.createElement('option');
        option.textContent = option.value = value;
        select.remove(option);
    }
    for (const value of externalService.getLanguagesList()) {
        const option = document.createElement('option');
        option.textContent = option.value = value;
        select.append(option);
    }
    regionsORlanguages = 'languages';
    return regionsORlanguages;
}

function selectOption() {
    switch (regionsORlanguages) {
        case 'regions':
            let allRegionsArray = [];
            allRegionsArray = externalService.getCountryListByRegion(select.value);
            return generateTableContent(allRegionsArray);
        case 'languages':
            let allLanguagesArray = [];
            allLanguagesArray = externalService.getCountryListByLanguage(select.value);
            return generateTableContent(allLanguagesArray);;
    }
}

const TABLE = `
<table >
    <thead>
        <tr>
            <th onclick="Vi.Table.sort.string(this)" id="country-name" >Country name</th>
            <th>Capital</th>
            <th>World Region</th>
            <th>Languages</th>
            <th onclick="Vi.Table.sort.number(this)" id="area">Area</th>
            <th>Flag</th>
        </tr>
    </thead>
    <tbody id="table"></tbody>
</table>`
appRoot.innerHTML += TABLE;

function generateTableContent(arr) {
    for (let countries of arr) {
        let tr = document.createElement('tr');

        let td1 = document.createElement('td');
        td1.innerHTML = countries.name;
        tr.appendChild(td1);

        let td2 = document.createElement('td');
        td2.innerHTML = countries.capital;
        tr.appendChild(td2);

        let td3 = document.createElement('td');
        td3.innerHTML = countries.region;
        tr.appendChild(td3);

        let td4 = document.createElement('td');
        td4.innerHTML = Object.values(countries.languages).join(', ');
        tr.appendChild(td4);

        let td5 = document.createElement('td');
        td5.innerHTML = countries.area;
        tr.appendChild(td5);

        let td6 = document.createElement('td');
        let img = document.createElement('img');
        img.src = countries.flagURL;
        td6.appendChild(img);
        tr.appendChild(td6);

        table.appendChild(tr);
    }
}

if (!Vi) {
    var Vi = {};
}
Vi.Table = Vi.Table || {};
Vi.Table.sort = function (th, getValue) {
    let className = th.classList.contains('sortAZ') ? 'sortZA' : 'sortAZ';
    let direction = className === 'sortAZ' ? 1 : -1;
    let table = th.closest('table');
    let tbody = table.tBodies[0];
    for (let th of table.rows[0].cells) {
        th.classList.remove('sortAZ', 'sortZA');
    }
    th.classList.add(className);
    let rows = Array.prototype.slice.call(table.rows, 1);
    rows.sort(function (rowA, rowB) {
        let valueA = getValue(rowA, th.cellIndex);
        let valueB = getValue(rowB, th.cellIndex);

        return valueA <= valueB ? -direction : direction;
    });
    for (let row in rows) {
        tbody.appendChild(rows[row]);
    }
}

Vi.Table.sort.number = function (th) {
    function getValue(tr, cellIndex) {
        return parseInt('0' + tr.children[cellIndex].innerText);
    }
    Vi.Table.sort(th, getValue);
}

Vi.Table.sort.string = function (th) {
    function getValue(tr, cellIndex) {
        let child = tr.children[cellIndex];
        return child.innerText.toUpperCase();
    }
    Vi.Table.sort(th, getValue);
}