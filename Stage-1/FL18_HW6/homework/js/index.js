function visitLink(path) { 
	let currentValue = parseInt(localStorage.getItem(path)) ? parseInt(localStorage.getItem(path)) : 0; 
	let newValue = currentValue + 1; 
	console.log(newValue); 
	localStorage.setItem(path, newValue);
}

function viewResults() { 
	const ul = document.createElement('ul'); 
	document.body.append(ul); 
	const numberOfLinks = document.getElementsByClassName('nav-link').length - 1; 
	console.log(numberOfLinks); 
	for (let i = 1; i <= numberOfLinks; i++) { 
		let counter = localStorage.getItem(`Page${i}`); 
		let li = document.createElement('li'); 
		counter === null ?
			li.innerHTML = `You haven't visited Page ${i} yet` :
			li.innerHTML = `You have visited Page ${i} ${counter} time(s)`; 
		ul.prepend(li); 
	} 
	localStorage.clear(); 
}