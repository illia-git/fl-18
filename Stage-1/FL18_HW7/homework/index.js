function getAge(dateString) { 
    const today = new Date(); 
    const birthDate = new Date(dateString); 
    let age = today.getFullYear() - birthDate.getFullYear(); 
    const monthDifference = today.getMonth() - birthDate.getMonth(); 
    if (monthDifference < 0 || monthDifference === 0 && today.getDate() < birthDate.getDate()) { 
        age -= 1;
    } 
    return age; 
} 
 
function getWeekDay(date) { 
    return new Date(date).toLocaleString('en-US', { weekday: 'long' }); 
} 
 
function getAmountDaysToNewYear() { 
    const today = new Date(); 
    const oneDay = 1000 * 60 * 60 * 24; 
    let xmas = new Date(today.getFullYear(), 11, 31); 
    if (today.getMonth() === 11 && today.getDate() > 31) { 
        xmas.setFullYear(xmas.getFullYear() + 1); 
    } 
    const daysLeft = Math.ceil((xmas.getTime() - today.getTime()) / oneDay); 
    return daysLeft; 
} 
 
function getProgrammersDay(year) { 
    const programmersDay = 256; 
    const programmersDateObj = new Date(year, 0, programmersDay); 
    const programmersDate = programmersDateObj.getDate(); 
    const programmersMonth = programmersDateObj.toLocaleString('en-US', { month: 'short' }); 
    const programmersYear = programmersDateObj.getFullYear(); 
    const programmersWeekDay = getWeekDay(programmersDateObj); 
    return `${programmersDate} ${programmersMonth}, ${programmersYear} (${programmersWeekDay})`; 
} 
 
function howFarIs(theDay) { 
    theDay = theDay.toLowerCase();
    theDay = theDay.charAt(0).toUpperCase() + theDay.slice(1); 
    const days = { 
        Monday: 1, 
        Tuesday: 2, 
        Wednesday: 3, 
        Thursday: 4, 
        Friday: 5, 
        Saturday: 6, 
        Sunday: 0 
    }; 
    const daysInWeek = 7; 
    const currDate = new Date(); 
    const currDay = currDate.getDay(); 
    const triggerDay = days[theDay]; 
    let diff = 0;
    triggerDay - currDay < 0 ? diff = triggerDay - currDay + daysInWeek : diff = triggerDay - currDay; 
    return diff === 0 ? `Hey, today is ${theDay} =)` : `It's ${diff} day(s) left till ${theDay}`;  
}

function isValidIdentifier(identifier) {
    const regex = `^([a-zA-Z_$][a-zA-Z\\d_$]*)$`;
    return identifier.match(regex) !== null;
}

function capitalize(testStr) {
    testStr = testStr.split(' ');
    for (let i = 0; i < testStr.length; i++) {
        testStr[i] = testStr[i][0].toUpperCase() + testStr[i].substr(1);
    }
    return testStr.join(' ');
}

function isValidAudioFile(audio) {
    const regex = /^[a-z]*\.(mp3|flac|alac|aac)$/;
    return regex.test(audio);
 }

 function getHexadecimalColors(color) {
    const regex = /#([a-f0-9]{3}){1,2}\b/gi;
    return regex.test(color) ? color.match(regex) : [];
}

function isValidPassword(pass) {
    const regex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/g;
    return regex.test(pass);
}

function addThousandsSeparators(numOrString) {
    const regex = /\B(?=(\d{3})+(?!\d))/g;
    return numOrString.toString().replace(regex, ',');
}

function getAllUrlsFromText (testLink) {
    const regex = new RegExp(
        `(https:[/][/]|http:[/][/]|www.)[a-zA-Z0-9-.]+.[a-zA-Z]{2,3}` +
        `(:[a-zA-Z0-9]*)?([a-zA-Z0-9-._?,'/+&amp;%$#=~])`,
        'g'
    );
    try {
        if (typeof testLink === 'undefined') {
            throw new Error('error');
        } else if (regex.test(testLink)) {
            return testLink.match(regex);
        } else {
            return [];
        }
    } catch (err) {
      return console.log(err.message);
    }
}