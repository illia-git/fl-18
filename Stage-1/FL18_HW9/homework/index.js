/* START TASK 1: Your code goes here */
const CELL_1 = document.getElementById('cell-1');
const CELL_2 = document.getElementById('cell-2');
const CELL_3 = document.getElementById('cell-3');
const CELL_4 = document.getElementById('cell-4');
const CELL_5 = document.getElementById('cell-5');
const CELL_6 = document.getElementById('cell-6');
const CELL_7 = document.getElementById('cell-7');
const CELL_8 = document.getElementById('cell-8');
const CELL_9 = document.getElementById('cell-9');

const CELLS_ARRAY = [CELL_1, CELL_2, CELL_3,
    CELL_4, CELL_5, CELL_6,
    CELL_7, CELL_8, CELL_9];

function changeRowOne() {
    switch (CELL_1.className) {
        case 'cell':
            CELL_1.className = CELL_2.className = CELL_3.className = 'cell-blue';
            break;
        case 'cell-blue':
            CELL_1.className = 'cell-yellow';
            break;
        case 'cell-green':
            CELL_1.className = 'cell-yellow';
            break;
        default:
            break;
    }
}

function changeRowTwo() {
    switch (CELL_4.className) {
        case 'cell':
            CELL_4.className = CELL_5.className = CELL_6.className = 'cell-blue';
            break;
        case 'cell-blue':
            CELL_4.className = 'cell-yellow';
            break;
        case 'cell-green':
            CELL_4.className = 'cell-yellow';
            break;
        default:
            break;
    }
}

function changeRowThree() {
    switch (CELL_7.className) {
        case 'cell':
            CELL_7.className = CELL_8.className = CELL_9.className = 'cell-blue';
            break;
        case 'cell-blue':
            CELL_7.className = 'cell-yellow';
            break;
        case 'cell-green':
            CELL_7.className = 'cell-yellow';
            break;
        default:
            break;
    }
}

function specialCell() {
    for (let i = 0; i < CELLS_ARRAY.length; i++) {
        if (CELLS_ARRAY[i].className === 'cell') {
            CELLS_ARRAY[i].className = 'cell-green';
        }
        CELL_6.className = 'cell-green';
    }
}

function changeToYellow(cell_id) {
    let elem = document.getElementById(cell_id);
    switch (elem.className) {
        case 'cell':
            elem.className = 'cell-yellow';
            break;
        case 'cell-green':
            elem.className = 'cell-yellow';
            break;
        case 'cell-blue':
            elem.className = 'cell-yellow';
            break;
        default:
            break;
    }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const PHONE_INPUT = document.getElementById('phone-input');
const PHONE_SUBMIT = document.getElementById('phone-submit');
const CORRECT_INFO = document.getElementById('correct');
const WRONG_INFO = document.getElementById('wrong');

function checkInput() {
    if (PHONE_INPUT.checkValidity()) {
        PHONE_SUBMIT.className = 'btn-submit-valid';
        PHONE_SUBMIT.disabled = false;
    } else {
        PHONE_SUBMIT.className = 'btn-submit-invalid';
        PHONE_SUBMIT.disabled = true;
    }
}

function sendData() {
    CORRECT_INFO.className = 'correct';
    WRONG_INFO.className = 'wrong-inv';
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */
const FIELD = document.getElementById('field');
const BALL = document.getElementById('ball');

FIELD.onclick = function (event) {
    const DIVIDE_TWO = 2;
    let fieldCoords = this.getBoundingClientRect();
    let ballCoords = {
        top: event.clientY - fieldCoords.top - FIELD.clientTop - BALL.clientHeight / DIVIDE_TWO,
        left: event.clientX - fieldCoords.left - FIELD.clientLeft - BALL.clientWidth / DIVIDE_TWO
    };

    if (ballCoords.top < 0) {
        ballCoords.top = 0;
    }

    if (ballCoords.left < 0) {
        ballCoords.left = 0;
    }

    if (ballCoords.left + BALL.clientWidth > FIELD.clientWidth) {
        ballCoords.left = FIELD.clientWidth - BALL.clientWidth;
    }

    if (ballCoords.top + BALL.clientHeight > FIELD.clientHeight) {
        ballCoords.top = FIELD.clientHeight - BALL.clientHeight;
    }

    BALL.style.left = ballCoords.left + 'px';
    BALL.style.top = ballCoords.top + 'px';
}

let aTeamScore = 0, bTeamScore = 0;
function scoreGoal(id) {
    const SCORE_MSG = document.getElementById('score-msg');
    const BASKET_A = document.getElementById('scoreboard-a');
    const BASKET_B = document.getElementById('scoreboard-b');
    const THREE_SEC = 3000;
    const HALF_SEC = 500;

    switch (id) {
        case 'basket-a':
            bTeamScore += 1;
            setTimeout(() => {
                SCORE_MSG.textContent = '';
            }, THREE_SEC);
            SCORE_MSG.textContent = 'Team B scores!';
            BASKET_B.textContent = `Team B: ${bTeamScore}`;
            ballPosition();
            break;
        case 'basket-b':
            aTeamScore += 1;
            setTimeout(() => {
                SCORE_MSG.textContent = '';
            }, THREE_SEC);
            SCORE_MSG.textContent = 'Team A scores!';
            BASKET_A.textContent = `Team A: ${aTeamScore}`;
            ballPosition();
            break;
        case 'main_field':
            setTimeout(() => {
                SCORE_MSG.textContent = '';
            }, THREE_SEC);
            SCORE_MSG.textContent = 'Miss!';
            ballPosition();
            break;
        default:
            break;
    }
    function ballPosition() {
        setTimeout(() => {
            BALL.style.top = '44%';
            BALL.style.left = '47%';
        }, HALF_SEC);
    }
}
/* END TASK 3 */
