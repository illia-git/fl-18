const getBigestNumber = require('../src/get-bigest-number');

const NUMBERS_SET = {
  44: 44,
  20: 20,
  13: 13,
  155: 155,
  31: 31
};

const NOT_ENOUGH_ARGS = {
  155: 155
};

const TOO_MANY_ARGS = {
  44: 44,
  20: 20,
  13: 13,
  155: 155,
  31: 31,
  50: 50,
  100: 100,
  39: 39,
  60: 60,
  10: 10,
  15: 15
};

const WRONG_TYPE = {
  44: 44,
  20: 20,
  13: 13,
  155: '155',
  31: 31
};

// import module `module.exports.someProperty = myCalc;`
// const myLocalVariable2 = require('./../src/index');

describe('CommonJS modules', () => {
  it('should be equal 155', async () => {
    const result = getBigestNumber(...Object.values(NUMBERS_SET));
    expect(result).toBe(NUMBERS_SET['155']);
  });

  it('should be not enough arguments', async () => {
    expect(() =>
      getBigestNumber(...Object.values(NOT_ENOUGH_ARGS))
    ).toThrowError(Error, 'Not enough arguments');
  });

  it('should be too many arguments', async () => {
    expect(() => getBigestNumber(...Object.values(TOO_MANY_ARGS))).toThrowError(
      Error,
      'Too many arguments'
    );
  });

  it('should be wrong argument type', async () => {
    expect(() => getBigestNumber(...Object.values(WRONG_TYPE))).toThrowError(
      Error,
      'Wrong argument type'
    );
  });
});
