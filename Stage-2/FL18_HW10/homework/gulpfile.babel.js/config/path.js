const PATH_SRC = './src';
const PATH_DST = './public';

export default {
    root: PATH_DST,

    html: {
        src: `${PATH_SRC}/html/*.html`,
        watch: `${PATH_SRC}/html/**/*.html`,
        dst: PATH_DST
    },

    scss: {
        src: `${PATH_SRC}/scss/*.{scss,sass}`,
        watch: `${PATH_SRC}/scss/**/*.{scss,sass}`,
        dst: `${PATH_DST}/css`
    },

    img: {
        src: `${PATH_SRC}/img/*.{png,jpg,jpeg,gif,svg}`,
        watch: `${PATH_SRC}/img/**/*.{png,jpg,jpeg,gif,svg}`,
        dst: `${PATH_DST}/img`
    },

    js: {
        src: `${PATH_SRC}/js/*.js`,
        watch: `${PATH_SRC}/js/**/*.js`,
        dst: `${PATH_DST}/js`
    }
}