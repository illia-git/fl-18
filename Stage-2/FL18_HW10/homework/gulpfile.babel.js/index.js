import gulp from 'gulp';

import app from './config/app.js';

import clear from './tasks/clear.js';
import server from './tasks/server.js';
import watcher from './tasks/watcher.js';

import html from './tasks/html.js';
import scss from './tasks/scss.js';
import js from './tasks/js.js';
//import img from './tasks/img.js';


const build = gulp.series(
    clear,
    gulp.parallel(html, scss, js)
);

const dev = gulp.series(
    build,
    gulp.parallel(watcher, server)
);

export {
    html,
    scss,
    js
};

export default app.isProd ? build : dev;