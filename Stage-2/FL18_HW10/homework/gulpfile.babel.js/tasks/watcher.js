import gulp from 'gulp';
import browserSync from 'browser-sync';

import path from '../config/path.js';

import html from './html.js';
import scss from './scss.js';
import js from './js.js';

export default () => {
    gulp.watch(path.html.watch, html).on('all', browserSync.reload);
    gulp.watch(path.scss.watch, scss).on('all', browserSync.reload);
    gulp.watch(path.js.watch, js).on('all', browserSync.reload);
}