import gulp from 'gulp';

import path from '../config/path.js';
import app from '../config/app.js';

import loadPlugins from 'gulp-load-plugins';
const gp = loadPlugins();

export default () => {
    return gulp.src(path.html.src)
        .pipe(gp.plumber({
            errorHandler: gp.notify.onError(error => ({
                title: 'HTML parsing error',
                message: error.message
            }))
        }))
        .pipe(gp.fileInclude())
        .pipe(gp.size({
            title: 'Before html-min'
        }))
        .pipe(gp.htmlmin(app.htmlmin))
        .pipe(gp.size({
            title: 'After html-min'
        }))
        .pipe(gulp.dest(path.html.dst))
}