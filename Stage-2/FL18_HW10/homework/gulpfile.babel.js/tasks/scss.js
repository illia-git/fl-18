import gulp from 'gulp';

import path from '../config/path.js';
import app from '../config/app.js';

import loadPlugins from 'gulp-load-plugins';
const gp = loadPlugins();

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

export default () => {
    return gulp.src(path.scss.src, {
            sourcemaps: app.isDev
        })
        .pipe(gp.plumber({
            errorHandler: gp.notify.onError(error => ({
                title: 'SCSS parsing error',
                message: error.message
            }))
        }))
        .pipe(gp.sassGlob())
        .pipe(sass())
        .pipe(gp.autoprefixer())
        .pipe(gp.shorthand())
        .pipe(gp.groupCssMediaQueries())
        .pipe(gp.size({
            title: 'Before css-min'
        }))
        .pipe(gulp.dest(path.scss.dst, {
            sourcemaps: app.isDev
        }))
        .pipe(gp.rename({
            suffix: '.min'
        }))
        .pipe(gp.csso())
        .pipe(gp.size({
            title: 'After css-min'
        }))
        .pipe(gulp.dest(path.scss.dst, {
            sourcemaps: app.isDev
        }))
}