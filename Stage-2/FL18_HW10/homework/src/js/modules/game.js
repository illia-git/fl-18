import arr from './array.js';
import el from './elements.js';
import reload_page from './reload_page.js';

export default () => {
    let move = 0,
        crosses = 0,
        zeroes = 0;

    crosses = localStorage.getItem('myCrosses');
    zeroes = localStorage.getItem('myZeroes');

    el.crosses_html.innerHTML = `Crosses: ${crosses}`;
    el.zeroes_html.innerHTML = `Zeroes: ${zeroes}`;

    const resetFunc = () => {
        crosses = 0;
        zeroes = 0;
        localStorage.setItem('myCrosses', crosses);
        localStorage.setItem('myZeroes', zeroes);
        el.crosses_html.innerHTML = `Crosses: ${crosses}`;
        el.zeroes_html.innerHTML = `Zeroes: ${zeroes}`;
    }

    const check = () => {
        for (let i in arr) {
            if (el.boxes[arr[i][0]].innerHTML === 'X' && el.boxes[arr[i][1]].innerHTML === 'X' && el.boxes[arr[i][2]].innerHTML === 'X') {
                el.crosses_html.innerHTML = `Crosses: ${++crosses}`
                localStorage.setItem('myCrosses', crosses);
                alert('Крестики выйграли');
                reload_page();
            } else if (el.boxes[arr[i][0]].innerHTML === 'O' && el.boxes[arr[i][1]].innerHTML === 'O' && el.boxes[arr[i][2]].innerHTML === 'O') {
                el.zeroes_html.innerHTML = `Crosses: ${++zeroes}`
                localStorage.setItem('myZeroes', zeroes);
                alert('Нолики выйграли');
                reload_page();
            }
        }
    }

    el.area.addEventListener('click', (e) => {
        const STATE = move % 2 === 0;
        e.target.innerHTML = STATE ? 'X' : 'O';
        move++;
        check();
    });

    el.reset.addEventListener('click', resetFunc);
}