export default () => {
    const SECOND = 1000;
    setTimeout(() => location.reload(), SECOND);
}