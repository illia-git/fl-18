const area = document.getElementById('area'),
    boxes = document.getElementsByClassName('box'),
    reset = document.getElementById('reset'),
    crosses_html = document.getElementById('crosses'),
    zeroes_html = document.getElementById('zeroes');

export default {
    area,
    boxes,
    reset,
    crosses_html,
    zeroes_html
}