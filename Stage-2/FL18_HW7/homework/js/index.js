/* eslint-disable no-magic-numbers */
$(document).ready(function () {
    let result = 0,
        currentEntry = '0',
        previousEntry = 0,
        operation = null;

    updateScreen(result);

    $('.button').on('click', function () {
        let buttonClicked = $(this).html();
        console.log(buttonClicked);

        if (buttonClicked === 'CE') {
            result = 0;
            currentEntry = '0';
            cleanError();
        } else if (isNumber(buttonClicked)) {
            currentEntry === '0' ?
                currentEntry = buttonClicked :
                currentEntry = currentEntry + buttonClicked;
        } else if (isOperator(buttonClicked)) {
            previousEntry = parseFloat(currentEntry);
            operation = buttonClicked;
            currentEntry = '';
        } else if (buttonClicked === '=') {
            let result = operate(previousEntry, currentEntry, operation);
            if (result === 'ERROR') {
                createError();
                currentEntry = result;
                operation = null;
            } else {
                calculatorHistory(previousEntry, operation, currentEntry, result);
                currentEntry = operate(previousEntry, currentEntry, operation);
                console.log(currentEntry.toString());
                operation = null;
            }
        } else {
            return null;
        }
        updateScreen(currentEntry);
    })
});

function calculatorHistory(a, operation, b, result) {
    a = a.toString();
    operation = operation.toString();
    b = b.toString();
    result = result.toString();
    let display = `${a} ${operation} ${b} = ${result}`;
    $('#history-ul')
        .append(`<li class="history-li">
                    <button class="history-btn__green"></button>
                    ${display}
                    <a href="#" class="remove-li">x</a>
                </li>`);

    $('.history-li:contains("48")').each(function () {
        $(this).html(
            $(this).html().replace(/48/g, '<span class="history-li__special">48</span>')
        );
    });

    $('.history-btn__green').on('click', function () {
        $(this).toggleClass('history-btn__red');
    });

    $('.remove-li').on('click', function () {
        $(this).parent('.history-li').remove();
    });
}

function isNumber(value) {
    return !isNaN(value);
}

function isOperator(value) {
    return value === '/' || value === '*' || value === '+' || value === '-';
}

function operate(a, b, operation) {
    a = parseFloat(a);
    b = parseFloat(b);
    let c = 0;
    const ERR = 'ERROR';
    if (operation === '/' && b === 0) {
        return ERR;
    }
    switch (operation) {
        case '+':
            c = a + b;
            break;
        case '-':
            c = a - b;
            break;
        case '*':
            c = a * b;
            break;
        case '/':
            c = a / b;
            break;
        default:
            break;
    }
    return c;
}

function updateScreen(displayValue) {
    displayValue = displayValue.toString();
    $('#result').html(displayValue.substring(0, 10));
}

function createError() {
    $('#result').addClass('error');
    $('.button').prop('disabled', true).addClass('disabled');
    $('#operator-delete').removeClass('disabled').prop('disabled', false);
}

function cleanError() {
    $('#result').removeClass('error');
    $('.button').prop('disabled', false).removeClass('disabled');
    $('#operator-backspace, #operator-plus-minus, #operator-percent, #operator-dot')
        .addClass('disabled')
        .prop('disabled', true);
}