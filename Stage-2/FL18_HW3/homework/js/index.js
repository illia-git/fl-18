'use strict';

class Pizza {
    constructor(size, type) {
        const REQUIRED_AMOUNT_OF_ARGUMENTS = 2;
        if (arguments.length !== REQUIRED_AMOUNT_OF_ARGUMENTS) {
            throw new PizzaException(`Required 2 arguments, given: ${arguments.length}`);
        } else if (!Pizza.allowedSizes.includes(size) || !Pizza.allowedTypes.includes(type)) {
            throw new PizzaException(`Invalid type`);
        } else {
            this.size = size;
            this.type = type;
            let totalExtrasPrice = 0;
            this.totalExtrasPrice = totalExtrasPrice;
            let totalExtras = [];
            this.totalExtras = totalExtras;
        }
    }

    addExtraIngredient(ingredient) {
        if (arguments.length !== 1) {
            throw new PizzaException(`Required 1 argument, given: ${arguments.length}`);
        } else if (!Pizza.allowedExtraIngredients.includes(ingredient)) {
            throw new PizzaException(`Invalid ingredient`);
        } else if (this.totalExtras.includes(ingredient.extra)) {
            throw new PizzaException(`Duplicate ingredient`);
        } else if (this.type.type === 'VEGGIE' && ingredient.extra === 'MEAT') {
            throw new PizzaException(`No meat with veggie`);
        } else {
            this.totalExtrasPrice += ingredient.price;
            this.totalExtras.push(ingredient.extra);
            return this.totalExtrasPrice, this.totalExtras;
        }
    }

    removeExtraIngredient(ingredient) {
        if (arguments.length !== 1) {
            throw new PizzaException(`Required 1 argument, given: ${arguments.length}`);
        } else if (!Pizza.allowedExtraIngredients.includes(ingredient)) {
            throw new PizzaException(`Invalid ingredient`);
        } else if (!this.totalExtras.includes(ingredient.extra)) {
            throw new PizzaException(`There is no such ingredient to remove`);
        } else {
            this.totalExtras.splice(this.totalExtras.indexOf(ingredient.extra), 1);
            this.totalExtrasPrice -= ingredient.price;
            return this.totalExtrasPrice, this.totalExtras;
        }
    }

    getSize() {
        return this.size.size;
    }

    getPrice() {
        let totalPizzaPrice = this.size.price + this.type.price + this.totalExtrasPrice;
        return totalPizzaPrice;
    }

    getPizzaInfo() {
        return `Size: ${this.getSize()};
                type: ${this.type.type};
                extra ingredients: (${this.totalExtras});
                price: ${this.getPrice()} UAH.`;
    }
}

Pizza.SIZE_S = { size: 'SMALL', price: 50 };
Pizza.SIZE_M = { size: 'MEDIUM', price: 75 };
Pizza.SIZE_L = { size: 'LARGE', price: 100 };

Pizza.TYPE_VEGGIE = { type: 'VEGGIE', price: 50 };
Pizza.TYPE_MARGHERITA = { type: 'MARGHERITA', price: 60 };
Pizza.TYPE_PEPPERONI = { type: 'PEPPERONI', price: 70 };

Pizza.EXTRA_TOMATOES = { extra: 'TOMATOES', price: 5 };
Pizza.EXTRA_CHEESE = { extra: 'CHEESE', price: 7 };
Pizza.EXTRA_MEAT = { extra: 'MEAT', price: 9 };

Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];

class PizzaException {
    constructor(log) {
        this.log = log;
    }
    log(log) {
        return log;
    }
}