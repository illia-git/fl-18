document.getElementById('refresh').addEventListener('click', () => window.location.reload());
document.getElementById('get-users').addEventListener('click', getUsers);
function getUsers() {
    showSpinner()
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(users => {
            let output = `<h1>Users Details</h1>`;
            users.forEach((value) => {
                output += `<ul id="${value.id}">
                                <li>
                                    <strong>Name is:</strong>
                                    <span id="sp-name-${value.id}" contentEditable=true>${value.name}</span>
                                </li>
                                <li>
                                    <strong>Username is:</strong>
                                    <span id="sp-username-${value.id}" contentEditable=true>${value.username}</span>
                                </li>
                                <li>
                                    <strong>Email is:</strong>
                                    <span id="sp-email-${value.id}" contentEditable=true>${value.email}</span>
                                </li>
                                <li>
                                    <button class="edit-btn" onclick="editUser(${value.id})">SEND EDIT</button>
                                    <button class="del-btn" onclick="deleteUser(${value.id})">DELETE</button>
                                </li>
                            </ul>`
            });
            document.getElementById('output').innerHTML = output;
        })
        .catch((err) => console.log(err));
}

function editUser(id) {
    showSpinner();
    const NAME = document.getElementById(`sp-name-${id}`).innerHTML;
    const USERNAME = document.getElementById(`sp-username-${id}`).innerHTML;
    const EMAIL = document.getElementById(`sp-email-${id}`).innerHTML
    const User = {
        'id': id,
        'name': `${NAME}`,
        'username': `${USERNAME}`,
        'email': `${EMAIL}`
    };
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        method: 'PUT',
        headers: { 'Content-type': 'application/json; charset=UTF-8' },
        body: JSON.stringify({ User })
    });
    console.log(JSON.stringify({ User }));
}

function deleteUser(id) {
    showSpinner();
    const NAME = document.getElementById(`sp-name-${id}`).innerHTML;
    const USERNAME = document.getElementById(`sp-username-${id}`).innerHTML;
    const EMAIL = document.getElementById(`sp-email-${id}`).innerHTML;
    const User = {
        'id': id,
        'name': `${NAME}`,
        'username': `${USERNAME}`,
        'email': `${EMAIL}`
    };
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        method: 'DELETE',
        headers: { 'Content-type': 'application/json; charset=UTF-8' },
        body: JSON.stringify({ User })
    });
    document.getElementById(id).remove();
}

function showSpinner() {
    const SPINNER = document.getElementById('spinner');
    const ONE_SECOND = 1000;
    SPINNER.className = 'show';
    setTimeout(() => {
        SPINNER.className = SPINNER.className.replace('show', '');
    }, ONE_SECOND);
}