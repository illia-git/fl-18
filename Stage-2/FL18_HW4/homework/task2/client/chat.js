let chat = {
    name: null,
    socket: null,
    ewrap: null,
    emsg: null,
    ego: null,
    init: () => {
        chat.ewrap = document.getElementById('chat-show');
        chat.emsg = document.getElementById('chat-msg');
        chat.ego = document.getElementById('chat-go');

        chat.name = prompt('What is your name?');
        if (chat.name === null || chat.name === '') {
            chat.name = 'Anon';
        }

        chat.socket = new WebSocket('ws://localhost:8080');

        chat.socket.addEventListener('open', () => {
            chat.controls(true);
            chat.send('Joined the chat room.');
        });

        chat.socket.addEventListener('message', (evt) => {
            chat.draw(evt.data);
        });

        chat.socket.addEventListener('close', () => {
            chat.controls(false);
            alert('Websocket connection lost!');
        });

        chat.socket.addEventListener('error', (err) => {
            chat.controls(false);
            console.log(err);
            alert('Websocket connection error!');
        });
    },

    controls: (enable) => {
        if (enable) {
            chat.emsg.disabled = false;
            chat.ego.disabled = false;
        } else {
            chat.emsg.disabled = true;
            chat.ego.disabled = true;
        }
    },

    send: (msg) => {
        if (msg === undefined) {
            msg = chat.emsg.value;
            chat.emsg.value = '';
        }
        chat.socket.send(JSON.stringify({
            name: chat.name,
            msg: msg
        }));
        return false;
    },

    draw: (msg) => {
        let today = new Date();
        let time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;

        msg = JSON.parse(msg);
        console.log(msg);

        let row = document.createElement('div');
        row.className = 'chatRow';
        row.innerHTML = `<div class="chatName">
                            ${msg['name']}
                        </div>
                        <div class="chatMsg">
                            ${msg['msg']}
                            <p class="time-text">${time}</p>
                        </div>`;
        chat.ewrap.appendChild(row);

        window.scrollTo(0, document.body.scrollHeight);
    }
};
window.addEventListener('DOMContentLoaded', chat.init);