let ws = require('ws');                     // eslint-disable-line
let wss = new ws.Server({ port: 8080 });
let users = {};

wss.on('connection', (socket) => {
    let id = 0;
    /*eslint-disable no-constant-condition*/
    while (true) {
        if (!users.hasOwnProperty(id)) {
            users[id] = socket;
            break;
        }
        id++;
    }

    socket.on('close', () => {
        delete users[id];
    });

    socket.on('message', (msg) => {
        let message = msg.toString();
        for (let u in users) {              // eslint-disable-line
            users[u].send(message);
        }
    });
});