/* eslint-disable no-magic-numbers */
const getMaxEvenElement = arr => Math.max(...arr.filter(x => x % 2 === 0));

let a = 3;
let b = 5;
a = a ^ b;
b = a ^ b;
a = a ^ b;

const getValue = str => str ?? '-';

const getObjFromArray = arr => Object.fromEntries(arr);

const addUniqueId = str => {
    return { ...str, id: Symbol() }
};

const getRegroupedObjects = obj => {
    let { name, ...user } = obj;
    const newObj = {
        university: user.details.university,
        user: { age: user.details.age, firstName: name, id: user.details.id }
    }
    return newObj;
}

const getArrayWithUniqueElements = array => [...new Set(array)];

const hideNumber = number => number.slice(-4).padStart(number.length, '*');

function add(a = 1, b = 1) {
    if (arguments.length !== 2) {
        throw new Error('Only 2 args!');
    } else {
        return a + b;
    }
}

function* generateIterableSequence(array = ['I', 'love', 'EPAM']) {
    for (let word of array) {
        yield word;
    }
}
const generatorObject = generateIterableSequence();
for (let value of generatorObject) {
    console.log(value);
}