import '/scss/normalize.scss'
import '/scss/main.scss'
import '/js/another.js'
import '/js/support.js'
import Paper from '/assets/paper.png';
import Rock from '/assets/rock.png';
import Scissors from '/assets/scissors.png';

const game = () => {
    let pScore = 0;
    let cScore = 0;

    //Start the Game
    const startGame = () => {
        const playBtn = document.querySelector('.intro button');
        const introScreen = document.querySelector('.intro');
        const match = document.querySelector('.match');

        playBtn.addEventListener('click', () => {
            introScreen.classList.add('fadeOut');
            match.classList.add('fadeIn');
        });
    };
    //Play Match
    const playMatch = () => {
        const options = document.querySelectorAll('.options button');
        const playerHand = document.querySelector('.player-hand');
        const computerHand = document.querySelector('.computer-hand');
        const hands = document.querySelectorAll('.hands img');

        hands.forEach(hand => {
            hand.addEventListener('animationend', function () {
                this.style.animation = '';
            });
        });
        //Computer Options
        const computerOptions = ['rock', 'paper', 'scissors'];

        options.forEach(option => {
            option.addEventListener('click', function () {
                //Computer Choice
                const computerNumber = Math.floor(Math.random() * 3);
                const computerChoice = computerOptions[computerNumber];

                setTimeout(() => {
                    //Here is where we call compare hands
                    compareHands(this.textContent, computerChoice);
                    //Update Images
                    switch (this.textContent) {
                        case 'rock':
                            playerHand.src = Rock;
                            break;
                        case 'paper':
                            playerHand.src = Paper;
                            break;
                        case 'scissors':
                            playerHand.src = Scissors;
                            break;
                        default: break;
                    }

                    switch (computerChoice) {
                        case 'rock':
                            computerHand.src = Rock;
                            break;
                        case 'paper':
                            computerHand.src = Paper;
                            break;
                        case 'scissors':
                            computerHand.src = Scissors;
                            break;
                        default: break;
                    }
                }, 2000);
                //Animation
                playerHand.style.animation = 'shakePlayer 2s ease';
                computerHand.style.animation = 'shakeComputer 2s ease';
            });
        });
    };

    const updateScore = () => {
        const WIN_SCORE = 3;
        const winner = document.querySelector('.winner');
        const playerScore = document.querySelector('.player-score p');
        const computerScore = document.querySelector('.computer-score p');
        if (pScore === WIN_SCORE) {
            playerScore.textContent = pScore;
            winner.textContent = 'Player Wins the Game';
            setTimeout(() => window.location.reload(), 3000);
        } else if (cScore === WIN_SCORE) {
            computerScore.textContent = cScore;
            winner.textContent = 'Computer Wins the Game';
            setTimeout(() => window.location.reload(), 3000);
        } else {
            playerScore.textContent = pScore;
            computerScore.textContent = cScore;
        }
    };

    const compareHands = (playerChoice, computerChoice) => {
        //Update Text
        const winner = document.querySelector('.winner');
        //Checking for a tie
        if (playerChoice === computerChoice) {
            winner.textContent = 'It is a tie';
            return;
        }
        //Check for Rock
        if (playerChoice === 'rock') {
            if (computerChoice === 'scissors') {
                winner.textContent = 'Player Wins';
                pScore++;
                updateScore();
                return;
            } else {
                winner.textContent = 'Computer Wins';
                cScore++;
                updateScore();
                return;
            }
        }
        //Check for Paper
        if (playerChoice === 'paper') {
            if (computerChoice === 'scissors') {
                winner.textContent = 'Computer Wins';
                cScore++;
                updateScore();
                return;
            } else {
                winner.textContent = 'Player Wins';
                pScore++;
                updateScore();
                return;
            }
        }
        //Check for Scissors
        if (playerChoice === 'scissors') {
            if (computerChoice === 'rock') {
                winner.textContent = 'Computer Wins';
                cScore++;
                updateScore();
                return;
            } else {
                winner.textContent = 'Player Wins';
                pScore++;
                updateScore();
                return;
            }
        }
    };

    //Is call all the inner function
    startGame();
    playMatch();
};

//start the game function
game();