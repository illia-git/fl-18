const changeBodyColor = () => {
    const NEW_COLOR = '#272944',
        BODY = document.getElementById('section');
    BODY.style.backgroundColor = NEW_COLOR;
}
setTimeout(changeBodyColor, 3000);
