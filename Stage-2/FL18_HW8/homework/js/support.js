const FIRST_LOG = 'I LOVE EPAM',
    SECOND_LOG = 'I DID MY HOMEWORK LOL ROFL',
    THIRD_LOG = 'I LISTEN TO BRITNEY SPEARS';

const logging = () => {
    console.log(FIRST_LOG);
    console.log(SECOND_LOG);
    console.log(THIRD_LOG);
}
logging();
